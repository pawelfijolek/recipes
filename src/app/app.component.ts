import { Component } from '@angular/core';
import { RecipesListServiceService } from './recipes-list-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RecipesListServiceService]
})
export class AppComponent {
  title = 'app';
}
