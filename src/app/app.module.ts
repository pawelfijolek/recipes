import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AlertModule} from 'ngx-bootstrap';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { RecipesComponent } from './recipes/recipes.component';
import {FormsModule} from '@angular/forms';
import { ReceipeDetailComponent } from './receipe-detail/receipe-detail.component';
import { AddIngredientsToReceipeComponent } from './add-ingredients-to-receipe/add-ingredients-to-receipe.component';

@NgModule({
  declarations: [
    AppComponent,
    RecipesComponent,
    ReceipeDetailComponent,
    AddIngredientsToReceipeComponent
  ],
  imports: [
    BrowserModule, AlertModule.forRoot(), HttpModule, FormsModule
  ],
  providers: [RecipesComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
