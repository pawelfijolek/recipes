import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'receipe-detail',
  templateUrl: './receipe-detail.component.html',
  styleUrls: ['./receipe-detail.component.css']
})
export class ReceipeDetailComponent implements OnInit {
  @Input() singleReceipie: {};
  @Output() favouriteOutput = new EventEmitter();
  @Output() newIngredients = new EventEmitter();
  ingredients: string[] = [];

  constructor() { }

  ngOnInit() {  }

  setFavourite() {
    if (this.singleReceipie != null) {
      this.favouriteOutput.emit({'receipe': this.singleReceipie});
    }
  }

  getAddedIngredients(event){
    console.log('-----event', event);
    //this.ingredients.push(event);
    this.newIngredients.emit({'singleReceipie': this.singleReceipie, 'event': event});
  }

}
