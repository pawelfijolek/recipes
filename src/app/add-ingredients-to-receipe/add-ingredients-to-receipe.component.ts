import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'add-ingredients-to-receipe',
  templateUrl: './add-ingredients-to-receipe.component.html',
  styleUrls: ['./add-ingredients-to-receipe.component.css']
})
export class AddIngredientsToReceipeComponent implements OnInit {
  ingredients: any [] = [];
  ingredientsToReceipe: any[] = [];
  ingredient: any;
  unitDictionary: any[]=[];
  amount: number;
  unit: string;
  @Output() addedIngredients = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.ingredients.push('FLOOR', 'EGGS', 'SALAD', 'BREAD', 'TOMATOES');
    this.unitDictionary.push('pound', 'pint', 'stone');
  }

  addIngredients() {
    var obj = {
      'ingredientsInClipboard' : null,
      'amount': null,
      'unit' : null
    };
    obj.ingredientsInClipboard = this.ingredient;
    obj.amount = this.amount;
    obj.unit = this.unit;
    this.ingredientsToReceipe.push(obj);
    const index: number = this.ingredients.indexOf(this.ingredient);
    if (index !== -1) {
      this.ingredients.splice(index, 1);
    }
  }

  addToReceipe() {
    if (this.ingredients !== null) {
      this.addedIngredients.emit(this.ingredientsToReceipe);
    }
  }



}
