import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIngredientsToReceipeComponent } from './add-ingredients-to-receipe.component';

describe('AddIngredientsToReceipeComponent', () => {
  let component: AddIngredientsToReceipeComponent;
  let fixture: ComponentFixture<AddIngredientsToReceipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIngredientsToReceipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIngredientsToReceipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
