import { TestBed, inject } from '@angular/core/testing';

import { RecipesListServiceService } from './recipes-list-service.service';

describe('RecipesListServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipesListServiceService]
    });
  });

  it('should be created', inject([RecipesListServiceService], (service: RecipesListServiceService) => {
    expect(service).toBeTruthy();
  }));
});
