import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import  'rxjs/add/operator/map';



@Injectable()
export class RecipesListServiceService {
	private _url: string = "assets/recipes.json"
	result: any[];
	constructor(private _http:Http) {
	  	
	}

	getResult() {
		debugger;
		return this._http.get(this._url)
			.map((response:Response) => response.json());
	}

}
