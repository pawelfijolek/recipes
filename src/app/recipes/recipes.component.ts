import { Component, OnInit } from '@angular/core';
import {RecipesListServiceService} from '../recipes-list-service.service';

@Component({
  selector: 'recipes-list',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
  providers: [RecipesListServiceService]
})
export class RecipesComponent implements OnInit {
  recipes = [];
  newRecipe: string;
  newRecipeObj: {};
  receipeToShowDetails: {};
  showDetail = false;

constructor(private _recipesList: RecipesListServiceService) {}

  ngOnInit() {
      this.recipes = [
      {id: 1, name: 'Beff Steak', bigAmount: true, ingrediens: ['Beef', 'salt'], favourite: false},
      {id: 2, name: 'Pasta', bigAmount: false, ingrediens: ['Beef', 'salt'], favourite: false},
      {id: 3, name: 'Egs', bigAmount: true, ingrediens: ['Beef', 'salt'], favourite: false}
    ];
  }

  deleteItem(receipe){
    const index = this.recipes.indexOf(receipe);
    this.recipes.splice(index, 1);
  }

  addItem(){
    const nextId = this.recipes.length;

    this.newRecipeObj = {
      'id': nextId + 1,
      'name': this.newRecipe,
      'bigAmount': true,
      'ingrediens' : {}
    };
    this.recipes.push(this.newRecipeObj);
  }

  showDetails(receipe){
    this.receipeToShowDetails = receipe;
    this.showDetail = true;
    console.log('details', this.receipeToShowDetails);
  }

  isFavourite(event){
    for (let i = 0; i < this.recipes.length;i++) {
      if (event.receipe.id === this.recipes[i].id) {
          this.recipes[i].favourite = event.receipe.favourite;
      }
    }
  }

  getNewIngredients(event){
    console.log('getNewIngredients', event);
    event.singleReceipie.igrediens.push(event.event[0]);
    //musisz tu przebudowac troche tablice ingrediens w receipe
    //to musi byc tablica obiektow: ingredients:string i amount: number
    //potem wyjmujesz z obiektu event obiekt singleReceipie ktory reprezentuje ten receipe na ktorym dodano ingredients
    //i pakujesz do tego nowego obiektu ingrediens i jego amount
  }


}
